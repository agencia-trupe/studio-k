<?php
class Contato extends Zend_Db_Table {

	protected $_name = 'contato';
	public $msg = array(
		'1' => 'Mensagem enviada com sucesso!',
		'2' => 'Ocorreu um erro ao enviar sua mensagem. <br/> Por favor, tente novamente'
	);

	public function Select() 
	{
		$select = $this->_db->select()
	 				   ->from('contato');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function getMessage($n=null)
	{
		return $n===null ? $this->msg : $this->msg[$n];
	}
}
?>