<?php
class Index extends Zend_Db_Table {

	protected $_name = 'slider_home';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from('slider_home')
	 				   ->order('ordem ASC')
	 				   ->limit(10);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectid($id) {
		$select = $this->_db->select()
	 				   ->from('slider_home')
	 				   ->where('id = ?',$id);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}
?>