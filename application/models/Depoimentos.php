<?php
class Depoimentos extends Zend_Db_Table {

	protected $_name = 'depoimentos';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from('depoimentos')
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}
?>