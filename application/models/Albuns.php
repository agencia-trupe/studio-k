<?php
class Albuns extends Zend_Db_Table {

	protected $_name = 'albuns';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from(array('a' => 'albuns'))
	 				   ->join(array('i' => 'imagens_albuns'),
                    		 'i.id_album = a.id AND i.capa = 1',
                    		 array('id_img'=>'i.id',
                    		 	   'caminho_img'=>'i.caminho_img'))
	 				   ->order('a.ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

}
class Imagensalbuns extends Zend_Db_Table {

	protected $_name = 'imagens_albuns';

	public function Select($id_album) {
		$select = $this->_db->select()
	 				   ->from('imagens_albuns')
	 				   ->where('id_album = ?',$id_album)
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}
?>