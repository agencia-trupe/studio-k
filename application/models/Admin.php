<?php
class Admin extends Zend_Db_Table {

	public function SelectUsuario($usuario) {
		$select = $this->_db->select()
	 				   ->from('usuario')
	 				   ->where('usuario = ?',$usuario);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Select($tabela) {
		$select = $this->_db->select()
	 				   ->from($tabela)
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectid($tabela,$id) {
		$select = $this->_db->select()
	 				   ->from($tabela)
	 				   ->where('id = ?',$id);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectordem($tabela,$ordem) {
		$select = $this->_db->select()
	 				   ->from($tabela)
	 				   ->where('ordem = ?',$ordem);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}

class Home extends Zend_Db_Table {

	protected $_name = 'slider_home';

}
class Empresa extends Zend_Db_Table {

	protected $_name = 'empresa';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from('empresa');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

}

class Casamentos extends Zend_Db_Table {

	protected $_name = 'casamentos';

	public function Selectcapa() {
		$select = $this->_db->select()
	 				   ->from(array('c' => 'casamentos'))
	 				   ->join(array('i' => 'imagens_casamento'),
                    		 		'i.id_casamento = c.id AND i.capa = 1',
                    		 		array('id_img'=>'i.id'));
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selectgaleria($id_casamento) {
		$select = $this->_db->select()
	 				   ->from('imagens_casamento')
	 				   ->where('id_casamento = ?',$id_casamento);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

}
class Imagemcasamento extends Zend_Db_Table {

	protected $_name = 'imagens_casamento';

	public function Selectgaleria($id_casamento) {
		$select = $this->_db->select()
	 				   ->from('imagens_casamento')
	 				   ->where('id_casamento = ?',$id_casamento)
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selectordem($ordem,$id_casamento) {
		$select = $this->_db->select()
	 				   ->from('imagens_casamento')
	 				   ->where('ordem = ?',$ordem)
	 				   ->where('id_casamento = ?',$id_casamento);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectdelete($ordem,$id_casamento) {
		$select = $this->_db->select()
	 				   ->from('imagens_casamento')
	 				   ->where('ordem > ?',$ordem)
	 				   ->where('id_casamento = ?',$id_casamento);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}
class Albuns extends Zend_Db_Table {

	protected $_name = 'albuns';

	public function Selectcapa() {
		$select = $this->_db->select()
	 				   ->from(array('a' => 'albuns'))
	 				   ->join(array('i' => 'imagens_albuns'),
                    		 		'i.id_albuns = a.id AND i.capa = 1',
                    		 		array('id_img'=>'i.id'));
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selectgaleria($id_album) {
		$select = $this->_db->select()
	 				   ->from('imagens_albuns')
	 				   ->where('id_album = ?',$id_album);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

}
class Imagemalbuns extends Zend_Db_Table {

	protected $_name = 'imagens_albuns';

	public function Selectgaleria($id_album) {
		$select = $this->_db->select()
	 				   ->from('imagens_albuns')
	 				   ->where('id_album = ?',$id_album)
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selectordem($ordem,$id_album) {
		$select = $this->_db->select()
	 				   ->from('imagens_albuns')
	 				   ->where('ordem = ?',$ordem)
	 				   ->where('id_album = ?',$id_album);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectdelete($ordem,$id_album) {
		$select = $this->_db->select()
	 				   ->from('imagens_albuns')
	 				   ->where('ordem > ?',$ordem)
	 				   ->where('id_album = ?',$id_album);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}

class Debutantes extends Zend_Db_Table {

	protected $_name = 'debutantes';

	public function Selectcapa() {
		$select = $this->_db->select()
	 				   ->from(array('d' => 'debutantes'))
	 				   ->join(array('i' => 'imagens_debutantes'),
                    		 		'i.id_debutantes = d.id AND i.capa = 1',
                    		 		array('id_img'=>'i.id'));
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selectgaleria($id_debutantes) {
		$select = $this->_db->select()
	 				   ->from('imagens_debutantes')
	 				   ->where('id_debutantes = ?',$id_debutantes);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

}
class Imagemdebutantes extends Zend_Db_Table {

	protected $_name = 'imagens_debutantes';

	public function Selectgaleria($id_album) {
		$select = $this->_db->select()
	 				   ->from('imagens_debutantes')
	 				   ->where('id_debutantes = ?',$id_album)
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selectordem($ordem,$id_debutantes) {
		$select = $this->_db->select()
	 				   ->from('imagens_debutantes')
	 				   ->where('ordem = ?',$ordem)
	 				   ->where('id_debutantes = ?',$id_debutantes);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectdelete($ordem,$id_debutantes) {
		$select = $this->_db->select()
	 				   ->from('imagens_debutantes')
	 				   ->where('ordem > ?',$ordem)
	 				   ->where('id_debutantes = ?',$id_debutantes);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}

class Films extends Zend_Db_Table {

	protected $_name = 'films';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from('films')
	 				   ->order(array('tipo ASC','ordem ASC'));
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selecttipo($tipo) {
		$select = $this->_db->select()
	 				   ->from('films')
	 				   ->where('tipo = ?',$tipo);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectordem($ordem,$tipo) {
		$select = $this->_db->select()
	 				   ->from('films')
	 				   ->where('ordem = ?',$ordem)
	 				   ->where('tipo = ?',$tipo);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

}

class Prewedding extends Zend_Db_Table {

	protected $_name = 'prewedding';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from('prewedding')
	 				   ->order(array('tipo ASC','ordem ASC'));
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selecttipo($tipo) {
		$select = $this->_db->select()
	 				   ->from('prewedding')
	 				   ->where('tipo = ?',$tipo);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectordem($ordem,$tipo) {
		$select = $this->_db->select()
	 				   ->from('prewedding')
	 				   ->where('ordem = ?',$ordem)
	 				   ->where('tipo = ?',$tipo);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

}
class Imagemprewedding extends Zend_Db_Table {

	protected $_name = 'imagens_prewedding';

	public function Selectgaleria($id_prewedding) {
		$select = $this->_db->select()
	 				   ->from('imagens_prewedding')
	 				   ->where('id_prewedding = ?',$id_prewedding)
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selectordem($ordem,$id_prewedding) {
		$select = $this->_db->select()
	 				   ->from('imagens_prewedding')
	 				   ->where('ordem = ?',$ordem)
	 				   ->where('id_prewedding = ?',$id_prewedding);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectdelete($ordem,$id_prewedding) {
		$select = $this->_db->select()
	 				   ->from('imagens_prewedding')
	 				   ->where('ordem > ?',$ordem)
	 				   ->where('id_prewedding = ?',$id_prewedding);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}

class Depoimentos extends Zend_Db_Table {

	protected $_name = 'depoimentos';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from('depoimentos')
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}
class Contato extends Zend_Db_Table {

	protected $_name = 'contato';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from('contato');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}
class Usuario extends Zend_Db_Table {

	protected $_name = 'usuario';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from('usuario')
	 				   ->where ('id <> ?', 1);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}
?>