<?php
class Prewedding extends Zend_Db_Table {

	protected $_name = 'prewedding';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from('prewedding')
	 				   ->order(array('tipo ASC','ordem ASC'));
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selecttipo($tipo) {
		$select = $this->_db->select()
	 				   ->from('prewedding')
	 				   ->where('tipo = ?',$tipo)
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectprimeiro() {
		$select = $this->_db->select()
	 				   ->from('prewedding')
	 				   // ->where('tipo = 1') // video
	 				   ->where('tipo = 0') // foto
	 				   ->order('ordem ASC')
	 				   ->limit(1);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectid($id) {
		$select = $this->_db->select()
	 				   ->from('prewedding')
	 				   ->where('id = ?',$id)
	 				   ->order('ordem ASC')
	 				   ->limit(1);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	
}
?>