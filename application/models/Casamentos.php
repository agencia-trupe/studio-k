<?php
class Casamentos extends Zend_Db_Table {

	protected $_name = 'casamentos';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from(array('c' => 'casamentos'))
	 				   ->join(array('i' => 'imagens_casamento'),
                    		 'i.id_casamento = c.id AND i.capa = 1',
                    		 array('id_img'=>'i.id',
                    		 	   'caminho_img'=>'i.caminho_img'))
	 				   ->order('c.ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selectid($id) {
		$select = $this->_db->select()
	 				   ->from('casamentos')
	 				   ->where('id = ?',$id);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}
class Imagenscasamento extends Zend_Db_Table {

	protected $_name = 'imagens_casamento';

	public function Select($id_casamento) {
		$select = $this->_db->select()
	 				   ->from('imagens_casamento')
	 				   ->where('id_casamento = ?',$id_casamento)
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectid($id) {
		$select = $this->_db->select()
	 				   ->from('imagens_casamento')
	 				   ->where('id = ?',$id);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}
?>