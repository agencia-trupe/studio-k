<?php
class Debutantes extends Zend_Db_Table {

	protected $_name = 'debutantes';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from(array('c' => 'debutantes'))
	 				   ->join(array('i' => 'imagens_debutantes'),
                    		 'i.id_debutantes = c.id AND i.capa = 1',
                    		 array('id_img'=>'i.id',
                    		 	   'caminho_img'=>'i.caminho_img'))
	 				   ->order('c.ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selectid($id) {
		$select = $this->_db->select()
	 				   ->from('debutantes')
	 				   ->where('id = ?',$id);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}
class Imagensdebutantes extends Zend_Db_Table {

	protected $_name = 'imagens_debutantes';

	public function Select($id_debutantes) {
		$select = $this->_db->select()
	 				   ->from('imagens_debutantes')
	 				   ->where('id_debutantes = ?',$id_debutantes)
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectid($id) {
		$select = $this->_db->select()
	 				   ->from('imagens_debutantes')
	 				   ->where('id = ?',$id);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}
?>