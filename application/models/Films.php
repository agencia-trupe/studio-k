<?php
class Films extends Zend_Db_Table {

	protected $_name = 'films';

	public function Select() {
		$select = $this->_db->select()
	 				   ->from('films')
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selecttipo($tipo) {
		$select = $this->_db->select()
	 				   ->from('films')
	 				   ->where('tipo = ?',$tipo)
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectprimeiro() {
		$select = $this->_db->select()
	 				   ->from('films')
	 				   ->where('tipo = 1')
	 				   ->order('ordem ASC')
	 				   ->limit(1);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectid($id) {
		$select = $this->_db->select()
	 				   ->from('films')
	 				   ->where('id = ?',$id)
	 				   ->order('ordem ASC')
	 				   ->limit(1);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	
}
?>