<?

class Imagemprewedding extends Zend_Db_Table {

	protected $_name = 'imagens_prewedding';

	public function Selectgaleria($id_prewedding) {
		$select = $this->_db->select()
	 				   ->from('imagens_prewedding')
	 				   ->where('id_prewedding = ?',$id_prewedding)
	 				   ->order('ordem ASC');
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}

	public function Selectordem($ordem,$id_prewedding) {
		$select = $this->_db->select()
	 				   ->from('imagens_prewedding')
	 				   ->where('ordem = ?',$ordem)
	 				   ->where('id_prewedding = ?',$id_prewedding);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
	public function Selectdelete($ordem,$id_prewedding) {
		$select = $this->_db->select()
	 				   ->from('imagens_prewedding')
	 				   ->where('ordem > ?',$ordem)
	 				   ->where('id_prewedding = ?',$id_prewedding);
	 	$results = $select->query()->fetchAll();
	 	
	 	return $results;
	}
}