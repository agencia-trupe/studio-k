<?php
require APPLICATION_PATH.'/models/Prewedding.php';
require APPLICATION_PATH.'/models/Imagemprewedding.php';

class PreweddingController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction(){
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];
        
        $lista = new Prewedding();

        $resultprimeiro = $lista->Selectprimeiro();

        foreach ($resultprimeiro as $campo => $valor) {
        	$this->view->id_atual = $valor['id'];
        }
        $result = $lista->Select();

        $result_tipo1 = $lista->Selecttipo(1);        
        $result_tipo2 = $lista->Selecttipo(0);

        $this->view->resultprimeiro = $resultprimeiro;
        $this->view->result_tipo1 = $result_tipo1;
        $this->view->result_tipo2 = $result_tipo2;

        $this->view->registros = count($result);
    }

    public function galeriaAction(){
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];

        $id = $this->getRequest()->getParam('id');

        $lista = new Imagemprewedding();
        $result = $lista->Selectgaleria($id);

        $this->view->result = $result;
    }


}

