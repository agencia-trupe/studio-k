<?php
require APPLICATION_PATH.'/models/Casamentos.php';

class CasamentosController extends Zend_Controller_Action
{

    public function indexAction(){
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];
        
        $lista = new Casamentos();
        $result = $lista->Select();

        $this->view->result = $result;
        $this->view->registros = count($result);
    }
    public function galeriaAction(){
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];
        
        $id = $this->getRequest()->getParam('id');

        $lista = new Imagenscasamento();
        $result = $lista->Select($id);

        $this->view->result = $result;

        $lista = new Casamentos();
        $result2 = $lista->Selectid($id);
        
        $this->view->result2 = $result2;
    }

}