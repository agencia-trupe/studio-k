<?php
require APPLICATION_PATH.'/models/Depoimentos.php';

class DepoimentosController extends Zend_Controller_Action
{

    public function indexAction(){
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];
        
    	$lista = new Depoimentos();
        $result = $lista->Select();

        $this->view->result = $result;
        $this->view->registros = count($result);
    }

}