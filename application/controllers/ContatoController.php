<?php
require APPLICATION_PATH.'/models/Contato.php';

class ContatoController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];

        $lista = new Contato();
        $result = $lista->Select();

        $this->view->result = $result;
        
        if($this->_hasParam('msg')){
            $this->view->msg = $lista->getMessage($this->_getParam('msg'));
        }

        $this->view->contato = true;
    }
    public function acaocontatoAction()
    {
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];
        
        if(APPLICATION_ENV=='development'){
            $configs = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini',APPLICATION_ENV);
            $configs = $configs->mailer;
            $tr = new Zend_Mail_Transport_Smtp($configs->smtp->server,$configs->smtp->toArray());
        } else {
            $tr = new Zend_Mail_Transport_Sendmail('-fnoreply@studiokeventos.com.br');
        }
        
        Zend_Mail::setDefaultTransport($tr);

        $email = "<b>Studio K - Novo contato</b><br><br>";
 		$email.='<b>Nome</b>: '.$this->getRequest()->getPost('nome')."<br>";
        $email.='<b>Email</b>: '.$this->getRequest()->getPost('email')."<br>";
        $email.='<b>Telefone</b>: '.$this->getRequest()->getPost('telefone')."<br>";
        $email.='<b>Data do evento</b>: '.$this->getRequest()->getPost('data_evento')."<br><br>";
        $email.='<b>Mensagem</b>: <br>'.nl2br($this->getRequest()->getPost('mensagem'))."<br><br>";

        $mail_to = $this->_hasParam('para') ? $this->_getParam('para') : 'contato@studiokeventos.com.br';

        $mail = new Zend_Mail();
        $mail->setBodyHtml(utf8_decode($email))
             ->setBodyText(strip_tags(str_replace('<br>',"\n",utf8_decode($email))))
             ->setSubject('Contato - Site Studio K');

        if(APPLICATION_ENV=='development'){
            $mail->setFrom('noreply@trupe.net', 'Trupe')
                 ->addTo('patrick@trupe.net', 'Contato (Trupe)')
                 ->setReplyTo($this->getRequest()->getPost('email'),utf8_decode($this->getRequest()->getPost('nome')));;
        } else {
            $mail->setFrom('noreply@studiokeventos.com.br', 'Studio k')
                 ->addTo($mail_to, 'Contato')
                 ->setReplyTo($this->getRequest()->getPost('email'),utf8_decode($this->getRequest()->getPost('nome')));;
                 // ->addBcc('patrick@trupe.net', 'Contato (Trupe)');
        }
        
        try{
            $mail->send();
            $this->_redirect('/contato/?msg=1');
        } catch(Exception $e){
            if(APPLICATION_ENV=='development') exit(var_dump($e->getMessage()));
            $this->_redirect('/contato/?msg=2');
        }
    }


}