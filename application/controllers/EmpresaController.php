<?php
require APPLICATION_PATH.'/models/Empresa.php';

class EmpresaController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];
        
        $lista = new Empresa();
        $result = $lista->Select();

        $this->view->result = $result;
        
    }


}