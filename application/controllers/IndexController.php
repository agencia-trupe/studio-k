<?php
require APPLICATION_PATH.'/models/Index.php';

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        
    }

    public function indexAction()
    {
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];
        
        $lista = new Index();
        $result = $lista->Select();

        $this->view->result = $result;
    }


}