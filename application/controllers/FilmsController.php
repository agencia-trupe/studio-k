<?php
require APPLICATION_PATH.'/models/Films.php';

class FilmsController extends Zend_Controller_Action
{

    public function indexAction(){
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];
        
        $lista = new Films();

        $resultprimeiro = $lista->Selectprimeiro();

        foreach ($resultprimeiro as $campo => $valor) {
        	$this->view->id_atual = $valor['id'];
        }
        $result = $lista->Select();

        $result_tipo1 = $lista->Selecttipo(1);        
        $result_tipo2 = $lista->Selecttipo(2);

        $this->view->resultprimeiro = $resultprimeiro;
        $this->view->result_tipo1 = $result_tipo1;
        $this->view->result_tipo2 = $result_tipo2;

        $this->view->registros = count($result);
    }

}