<?php
require APPLICATION_PATH.'/models/Albuns.php';

class AlbunsController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];

    	$lista = new Albuns();
        $result = $lista->Select();

        $this->view->result = $result;
        $this->view->registros = count($result);
    }
    public function galeriaAction(){
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];

        $id = $this->getRequest()->getParam('id');

        $lista = new Imagensalbuns();
        $result = $lista->Select($id);

        $this->view->result = $result;
    }


}