<?php
require APPLICATION_PATH.'/models/Debutantes.php';

class DebutantesController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];
        
        $lista = new Debutantes();
        $result = $lista->Select();

        $this->view->result = $result;
        $this->view->registros = count($result);
    }

    public function galeriaAction()
    {
        $url = explode("/", $this->getRequest()->getRequestUri());
        $this->view->url = $url[1];
        
        $id = $this->getRequest()->getParam('id');

        $lista = new Imagensdebutantes();
        $result = $lista->Select($id);

        $this->view->result = $result;

        $lista2 = new Debutantes();
        $result2 = $lista2->Selectid($id);

        $this->view->result2 = $result2;
    }


}