<?php
require APPLICATION_PATH.'/models/Admin.php';

class AdminController extends Zend_Controller_Action
{
    public function init()
    {
        ini_set('session.save_path', 'tmp');
    }
    

    public function indexAction(){
        $this->view->erro = $this->getRequest()->getParam('e');

    }
    public function loginAction(){

        $usuario = $this->getRequest()->getPost('usuario', null);
        $senha = $this->getRequest()->getPost('senha', null);
        
        $lista = new Admin();
        $result = $lista->SelectUsuario($usuario);

        foreach ($result as $row) {
            $usuariodb = $row['usuario'];
            $senhadb = $row['senha'];
        }
        
        if(empty($senha) || empty($usuario)){
            $this->_redirect('/admin/?e=1');         
        }
        else if($usuario != $usuariodb || md5($senha) != $senhadb){
            $this->_redirect('/admin/?e=2'); 
        }
        else{
            $sessao = new Zend_Session_Namespace('Zend_Auth');
            $sessao->usuario = $usuario;
            $this->_redirect('/admin/principal');
        }
    }

    public function principalAction(){
        $this->checasessao();
    }  

    /*** home ***/  

    public function homeAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        
        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');

        $lista = new Admin();
        $result = $lista->Select('slider_home');

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(20);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->registros = count($result);
    }

    public function adicionahomeAction(){
        $this->checasessao();
    }  

    public function acaoadicionahomeAction(){
        $this->checasessao();
        $erro == 0;

        $caminho = UPLOAD_PATH . "/home/";
        
        $upload = new Zend_File_Transfer_Adapter_Http();

        $upload->addValidator('FilesSize', false, array('max' => '12MB'))
                   ->addValidator('Extension', false, array('png','gif','jpeg','jpg',))       
                   ->setDestination($caminho);
        
        if (count($upload->getFileName()) != 0){

            $ext = strtolower(pathinfo($upload->getFileName(), PATHINFO_EXTENSION)); 

            $arquivo = md5(date(YmdHis)).md5('home').'.'.$ext;

            $nomearquivo = "tmp_".$arquivo;
            
            $upload->addFilter('Rename', $caminho.$nomearquivo,$upload->getFileName());
            
            if ($upload->receive()){}
            else {
                $erro = 5;
                foreach($upload -> getMessages() as $key=> $campo){
                   
                    switch ($key) {
                        case 'fileExtensionFalse':
                            $erro = 3;
                        break;
                        case 'fileUploadErrorIniSize':
                            $erro = 4;
                        break;
                    }
                }
            } 
            if ($erro == 0) {

                $caminho_imagem = $caminho.$nomearquivo;

                $image1 = Zend_Pdf_Image::imageWithPath($caminho_imagem);
                $largura_img = $image1->getPixelWidth();
                $altura_img = $image1->getPixelHeight();

                $caminho_imagem_novo = $caminho.md5(date(YmdHis)).md5('home').'.'.$ext;
                
                $w = 1024; $h = 480;
                
                require_once('library/Thumb/Factory.php');
                $thumb = Php_Thumb_Factory::create($caminho_imagem);

                // resize com crop
                $thumb->adaptiveResize($w,$h);
                // resize sem crop
                // $thumb->resize($w,$h);

                $thumb->save($caminho_imagem_novo);

                unlink($caminho_imagem);

                $lista = new Admin();
                $result = $lista->Select('slider_home');

                $registros = count($result)+1;
                $link = addslashes($this->_getParam('link'));

                $data = array(
                        'caminho_imagem'    => $arquivo,
                        'link'              => $link,
                        'ordem'             => $registros
                    );

                $banco = new Home();

                $banco->insert($data);
                
                $this->_redirect('/admin/home/msg/1');
            }
            else {
                $this->_redirect('/admin/home/erro/'.$erro);
            }
        }        
    }

    public function deletahomeAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $lista = new Admin();
        $result = $lista->Selectid('slider_home',$id);        

        foreach ($result as $campo => $valor) {
            $caminho_imagem = $valor['caminho_imagem'];
        }
        unlink(UPLOAD_PATH . "/home/".$caminho_imagem);

        $banco = new Home();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/home/msg/3');
    }

    public function alterahomeAction(){
        $this->checasessao();

        $alteracao = $this->getRequest()->getParam('alteracao');
        $id = $this->getRequest()->getParam('id');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('slider_home',$id);        

        foreach ($result as $campo => $valor) {
            $ordem = $valor['ordem'];
        }

        if ($alteracao == 'subir') {
            $novaordem = $ordem -1;
        }
        else{
            $novaordem = $ordem +1;
        }

        $result2 = $lista->Selectordem('slider_home',$novaordem);

        foreach ($result2 as $campo => $valor) {
            $id_novo = $valor['id'];
        }

        $data = array(
            'ordem'        => $novaordem
        );
        $data2 = array(
            'ordem'        => $ordem
        );

        $banco = new Home();
         
        $banco->update($data, 'id = '.$id);

        $banco->update($data2, 'id = '.$id_novo);
        
        
        $result = $lista->Select('slider_home');

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(20);

        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
    }

    public function ordenahomeAction(){
        $this->checasessao();

        $ids = $this->getRequest()->getParam('ids');
        $ids = explode(',', $ids);
        $pagina = $this->getRequest()->getParam('pagina');
        
        $lista = new Admin();
        $banco = new Home();
        
        $i = 0; foreach ($ids as $id) { $i++;
            $banco->update(array('ordem'=>$i), 'id = '.$id);
        }
        
        $result = $lista->Select('slider_home');

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(20);

        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
    }

    public function editahomeAction(){
        $this->checasessao();

        $id = $this->getRequest()->getParam('id');
        
        $lista = new Admin();
        $result = $lista->Selectid('slider_home',$id);

        $this->view->result = $result;
    }

    public function acaoeditahomeAction(){
        $this->checasessao();
        $erro == 0; $arquivo = null;
        $id = $this->getRequest()->getPost('id');

        $caminho = UPLOAD_PATH . "/home/";

        $upload = new Zend_File_Transfer_Adapter_Http();

        $upload->addValidator('FilesSize', false, array('max' => '12MB'))
                   ->addValidator('Extension', false, array('png','gif','jpeg','jpg',))       
                   ->setDestination($caminho);

        if (count($upload->getFileName()) != 0){
            $ext = strtolower(pathinfo($upload->getFileName(), PATHINFO_EXTENSION)); 

            $arquivo = md5(date(YmdHis)).md5('home').'.'.$ext;

            $nomearquivo = "tmp_".$arquivo;
            
            $upload->addFilter('Rename', $caminho.$nomearquivo,$upload->getFileName());
            
            if ($upload->receive()){
            } else {
                $erro = 5;
                foreach($upload -> getMessages() as $key=> $campo){
                   
                    switch ($key) {
                        case 'fileExtensionFalse':
                            $erro = 3;
                        break;
                        case 'fileUploadErrorIniSize':
                            $erro = 4;
                        break;
                    }
                }
            } 
            if ($erro == 0) {

                $caminho_imagem = $caminho.$nomearquivo;

                $image1 = Zend_Pdf_Image::imageWithPath($caminho_imagem);
                $largura_img = $image1->getPixelWidth();
                $altura_img = $image1->getPixelHeight();

                $caminho_imagem_novo = $caminho.md5(date(YmdHis)).md5('home').'.'.$ext;
                
                $w = 1024; $h = 480;
                
                require_once('library/Thumb/Factory.php');
                $thumb = Php_Thumb_Factory::create($caminho_imagem);

                // resize com crop
                $thumb->adaptiveResize($w,$h);
                // resize sem crop
                // $thumb->resize($w,$h);

                $thumb->save($caminho_imagem_novo);

                unlink($caminho_imagem);
            } else {
                $this->_redirect('/admin/home/erro/'.$erro);
            }
        }

        $link = addslashes($this->_getParam('link'));
        $data = array('link' => $link);
        if($arquivo!==null) $data['caminho_imagem'] = $arquivo;

        $banco = new Home();

        $banco->update($data, 'id = '.$id);
        
        $this->_redirect('/admin/home/msg/2');
    }
    
    /*** empresa ***/

    public function empresaAction(){
        $this->checasessao();
        
        $lista = new Empresa();
        $result = $lista->Select();

        $this->view->result = $result;
    }

    public function acaoempresaAction(){
        $this->checasessao();
        
        $id = $this->getRequest()->getPost('id');

        $caminho_video = $this->getRequest()->getPost('video');
        // $caminho_video = str_replace('http://www.youtube.com/watch?v=','',$caminho_video);
        // $caminho_video = str_replace('https://www.youtube.com/watch?v=','',$caminho_video);
        // $caminho_video = str_replace('http://youtu.be/','',$caminho_video);

        $video = explode('?',$caminho_video);
        $data = array(
                'texto'    => $this->getRequest()->getPost('texto'),
                // 'video'    => $video[0],
                'video'    => $caminho_video,
            );

        $banco = new Empresa();

        $banco->update($data, 'id = '.$id);
        
        $this->_redirect('/admin/empresa/msg/1');
    }

    /*** casamentos ***/

    public function casamentosAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        
        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');
        
        $lista = new Casamentos();
        $lista_capa = $lista->Selectcapa();

        $lista = new Admin();
        $result = $lista->Select('casamentos');

        $capa = '';
        $result_filtro = array();
        foreach ($result as $campo => $valor) {
            foreach ($lista_capa as $campo_capa => $valor_capa) {
                echo 1;
                if ($valor['id'] == $valor_capa['id']){
                    $capa = 1;
                }
            }
            $result_filtro[] = array(
                                    'id' => $valor["id"], 
                                    'nome' => $valor["nome"], 
                                    'ordem' => $valor["ordem"], 
                                    'capa' => $capa
                            );
            $capa ='';

        }

        $paginator = Zend_Paginator::factory($result_filtro);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->registros = count($result);
    }    

    public function adicionacasamentoAction(){
        $this->checasessao();
    }  

    public function acaoadicionacasamentoAction(){
        $this->checasessao();

        $lista = new Admin();
        $result = $lista->Select('casamentos');

        $registros = count($result)+1;

        $data = array(
                'nome'    => $this->getRequest()->getPost('nome'),
                'ordem'   => $registros
            );

        $banco = new Casamentos();

        $banco->insert($data);
        
        $this->_redirect('/admin/casamentos/msg/1');
    }


    public function editacasamentoAction(){
        $this->checasessao();

        $id = $this->getRequest()->getParam('id');
        
        $lista = new Admin();
        $result = $lista->Selectid('casamentos',$id);

        $this->view->result = $result;
    }   

    public function acaoeditacasamentoAction(){
        $this->checasessao();

        $id = $this->getRequest()->getPost('id');

        $data = array(
                'nome'    => $this->getRequest()->getPost('nome')
            );

        $banco = new Casamentos();

        $banco->update($data, 'id = '.$id);
        
        $this->_redirect('/admin/casamentos/msg/2');
    }
    public function alteracasamentosAction(){
        $this->checasessao();

        $alteracao = $this->getRequest()->getParam('alteracao');
        $id = $this->getRequest()->getParam('id');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('casamentos',$id);        

        foreach ($result as $campo => $valor) {
            $ordem = $valor['ordem'];
        }

        if ($alteracao == 'subir') {
            $novaordem = $ordem -1;
        }
        else{
            $novaordem = $ordem +1;
        }

        $result2 = $lista->Selectordem('casamentos',$novaordem);

        foreach ($result2 as $campo => $valor) {
            $id_novo = $valor['id'];
        }

        $data = array(
            'ordem'        => $novaordem
        );
        $data2 = array(
            'ordem'        => $ordem
        );

        $banco = new Casamentos();
         
        $banco->update($data, 'id = '.$id);

        $banco->update($data2, 'id = '.$id_novo);
        
        
        $result = $lista->Select('casamentos');

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
    }

    public function deletacasamentoAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $lista = new Casamentos();
        $result = $lista->Selectgaleria($id);        

        foreach ($result as $campo => $valor) { 
            unlink(UPLOAD_PATH . "/casamento/".$valor['caminho_img']);
            unlink(UPLOAD_PATH . "/casamento/tmb_".$valor['caminho_img']);
            
            $banco = new Imagemcasamento();
            $banco->delete('id = '.$valor['id']);
        }

        $banco = new Casamentos();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/casamentos/msg/3');
    }

    /** Galeria casamento**/

    public function casamentogaleriaAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        $id = $this->getRequest()->getParam('id');
        $this->view->id_galeria = $id;

        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');

        $lista = new Admin();
        $result1 = $lista->Selectid('casamentos',$id);

        foreach ($result1 as $campo => $valor) {
            $this->view->nome_galeria = $valor['nome'];
        }

        $lista = new Imagemcasamento();
        $result = $lista->Selectgaleria($id);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->registros = count($result);
    }  

    public function adicionacasamentogaleriaAction(){
        $this->checasessao();

        $this->view->id_galeria = $this->getRequest()->getParam('id_galeria');
    }  

    public function acaoadicionacasamentogaleriaAction(){
        $this->checasessao();

        $erro == 0;

        $id_galeria = $this->getRequest()->getPost('id_galeria');

        $caminho = UPLOAD_PATH . "/casamento/";
        
        $upload = new Zend_File_Transfer_Adapter_Http();

        $upload->addValidator('FilesSize', false, array('max' => '12MB'))
               ->addValidator('Extension', false, array('png','gif','jpeg','jpg'))       
               ->setDestination($caminho);
        
        if (count($upload->getFileName()) != 0){

            $ext = strtolower(pathinfo($upload->getFileName(), PATHINFO_EXTENSION)); 

            $nome_imagem = md5(date(YmdHis)).md5('casamento').'.'.$ext;

            $nomearquivo = "tmp_".$nome_imagem;

            
            $upload->addFilter('Rename', $caminho.$nomearquivo,$upload->getFileName());
            
            if ($upload->receive()){}
            else {
                $erro = 5;
                foreach($upload -> getMessages() as $key=> $campo){
                   
                    switch ($key) {
                        case 'fileExtensionFalse':
                            $erro = 3;
                        break;
                        case 'fileUploadErrorIniSize':
                            $erro = 4;
                        break;
                    }
                }
            } 
            if ($erro == 0) {

                $caminho_imagem = $caminho.$nomearquivo;

                $image1 = Zend_Pdf_Image::imageWithPath($caminho_imagem);
                $largura_img = $image1->getPixelWidth();
                $altura_img = $image1->getPixelHeight();

                $caminho_imagem_novo = $caminho.$nome_imagem;
                $tbm_src = $caminho."tmb_".$nome_imagem;
                 
                

                // largura ($w) e altura ($h) do thumbnail

                $w_tmb = 290;
                $h_tmb = 90;

                require_once('library/Thumb/Factory.php');
                $thumb = Php_Thumb_Factory::create($caminho_imagem);

                if($largura_img>=$altura_img){
                    $w = 900; 
                    $h = 600;
                    $h_novo = ceil(($w*$altura_img)/$largura_img);

                    if($h_novo > $h){
                        $thumb->adaptiveResize($w,$h); // com crop
                    }
                    else{
                        $thumb->resize($w,$h_novo); //sem crop
                    }
                }
                else{
                    $w = 900; 
                    $h = 600;  

                    $w_novo = ceil(($h*$largura_img)/$altura_img);

                    if($w_novo > $w){
                        $thumb->adaptiveResize($w,$h); // com crop
                    }
                    else{
                        $thumb->resize($w_novo,$h); //sem crop
                    }
                }

                $thumb->save($caminho_imagem_novo);

                $thumb->adaptiveResize($w_tmb,$h_tmb);
                $thumb->save($tbm_src);

                unlink($caminho_imagem);

                $lista = new Imagemcasamento();
                $result = $lista->Selectgaleria($id_galeria);

                $registros = count($result)+1;

                $data = array(
                    'caminho_img'    => $nome_imagem,
                    'id_casamento'   => $this->getRequest()->getPost('id_galeria'),
                    'ordem'          => $registros
                );

                $banco = new Imagemcasamento();

                $banco->insert($data);
                
                $this->_redirect('/admin/casamentogaleria/id/'.$id_galeria.'/msg/1');
            }
            else{
                $this->_redirect('/admin/casamentogaleria/id/'.$id_galeria.'/erro/'.$erro);
            }
        }        
    }

   /* public function acaocropadicionacasamentogaleriaAction(){
        $ratio = round(900/$this->getRequest()->getPost('w'),3);
        $targ_w = 900;
        $targ_h = ceil($this->getRequest()->getPost('h')*$ratio);
        $jpeg_quality = 90;

        $src =  UPLOAD_PATH . "/casamento/".$this->getRequest()->getPost('img');

        $novo_src = UPLOAD_PATH . "/casamento/".substr($this->getRequest()->getPost('img'), 4);
        $tbm_src = UPLOAD_PATH . "/casamento/tmb_".substr($this->getRequest()->getPost('img'), 4);

        $ext = strtolower(pathinfo($this->getRequest()->getPost('img'), PATHINFO_EXTENSION)); 

        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
        
        switch ($ext) {
            case 'jpg':
                $img_r = imagecreatefromjpeg($src);
            break;
            case 'gif':
                $img_r = imagecreatefromgif($src);
            break;
            case 'png':
                $img_r = imagecreatefrompng($src);
            break;
            case 'jpeg':
                $img_r = imagecreatefromjpeg($src);
            break;
        }

        imagecopyresampled($dst_r,$img_r,0,0,$this->getRequest()->getPost('x'),$this->getRequest()->getPost('y'),$targ_w,$targ_h,$this->getRequest()->getPost('w'),$this->getRequest()->getPost('h'));

        imagejpeg($dst_r,$novo_src,$jpeg_quality);


        switch ($ext) {
            case 'jpg':
                $img_r_n = imagecreatefromjpeg($novo_src);
            break;
            case 'gif':
                $img_r_n = imagecreatefromjpeg($novo_src);
            break;
            case 'png':
                $img_r_n = imagecreatefromjpeg($novo_src);
            break;
            case 'jpeg': 
                $img_r_n = imagecreatefromjpeg($novo_src);
            break;
        }

        $dst_r_tmb = ImageCreateTrueColor( 290, 90 );

        imagecopyresampled($dst_r_tmb,$img_r_n,0,0,0,95,290,279,900,600);
        imagejpeg($dst_r_tmb,$tbm_src,$jpeg_quality);

        unlink($src);


        $lista = new Imagemcasamento();
        $result = $lista->Selectgaleria($this->getRequest()->getPost('id_galeria'));

        $registros = count($result)+1;

        $data = array(
                'caminho_img'    => substr($this->getRequest()->getPost('img'), 4),
                'id_casamento'   => $this->getRequest()->getPost('id_galeria'),
                'ordem'          => $registros
            );

        $banco = new Imagemcasamento();

        $banco->insert($data);
        
        $this->_redirect('/admin/casamentogaleria/id/'.$this->getRequest()->getPost('id_galeria'));
    }*/
    public function alteracasamentogaleriaAction(){
        $this->checasessao();

        $alteracao = $this->getRequest()->getParam('alteracao');
        $id = $this->getRequest()->getParam('id');
        $id_galeria = $this->getRequest()->getParam('id_galeria');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('imagens_casamento',$id);        

        foreach ($result as $campo => $valor) {
            $ordem = $valor['ordem'];
        }

        if ($alteracao == 'subir') {
            $novaordem = $ordem -1;
        }
        else{
            $novaordem = $ordem +1;
        }
        $lista = new Imagemcasamento();
        $result2 = $lista->Selectordem($novaordem,$id_galeria);

        foreach ($result2 as $campo => $valor) {
            $id_novo = $valor['id'];
        }

        $data = array(
            'ordem'        => $novaordem
        );
        $data2 = array(
            'ordem'        => $ordem
        );

        $banco = new Imagemcasamento();
         
        $banco->update($data, 'id = '.$id);

        $banco->update($data2, 'id = '.$id_novo);
        
        
        $result = $lista->Selectgaleria($id_galeria);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);
                  
        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
        $this->view->id_galeria = $id_galeria;
    }
    public function selecionacasamentogaleriaAction(){
        $this->checasessao();

        $id = $this->getRequest()->getParam('id');
        $id_galeria = $this->getRequest()->getParam('id_galeria');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('imagens_casamento',$id);        

        
        $data = array(
            'capa'        => 0
        );
        $data2 = array(
            'capa'        => 1
        );

        $banco = new Imagemcasamento();
         
        $banco->update($data, array('capa = ?' => 1 , 'id_casamento = ?' => $id_galeria));

        $banco->update($data2, 'id = '.$id);
        
        $lista = new Imagemcasamento();
        $result = $lista->Selectgaleria($id_galeria);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);
                  
        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
        $this->view->id_galeria = $id_galeria;
    }

    public function deletacasamentogaleriaAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $lista = new Admin();
        $result = $lista->Selectid('imagens_casamento',$id);        

        foreach ($result as $campo => $valor) {
            $caminho_img = $valor['caminho_img'];
            $id_galeria = $valor['id_casamento'];
            $ordem = $valor['ordem'];
        }
        unlink(UPLOAD_PATH . "/casamento/".$caminho_img);
        unlink(UPLOAD_PATH . "/casamento/tmb_".$caminho_img);

        $lista = new Imagemcasamento();
        $result = $lista->Selectdelete($ordem,$id_galeria);

        foreach ($result as $campo => $valor) {
            $data = array('ordem' => $valor['ordem']-1);

            $lista->update($data, 'id = '.$valor['id']);
        }

        $banco = new Imagemcasamento();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/casamentogaleria/id/'.$id_galeria.'/msg/3');
    }
    /*** Albuns ***/

    public function albunsAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        
        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');
        
        $lista = new Casamentos();
        $lista_capa = $lista->Selectcapa();

        $lista = new Admin();
        $result = $lista->Select('albuns');

        $capa = '';
        $result_filtro = array();
        foreach ($result as $campo => $valor) {
            foreach ($lista_capa as $campo_capa => $valor_capa) {
                echo 1;
                if ($valor['id'] == $valor_capa['id']){
                    $capa = 1;
                }
            }
            $result_filtro[] = array(
                                    'id' => $valor["id"], 
                                    'nome' => $valor["nome"], 
                                    'ordem' => $valor["ordem"], 
                                    'capa' => $capa
                            );
            $capa ='';

        }

        $paginator = Zend_Paginator::factory($result_filtro);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->registros = count($result);
    }     

    public function adicionaalbunsAction(){
        $this->checasessao();
    }  

    public function acaoadicionaalbunsAction(){
        $this->checasessao();

        $lista = new Admin();
        $result = $lista->Select('albuns');

        $registros = count($result)+1;

        $data = array(
                'nome'    => $this->getRequest()->getPost('nome'),
                'ordem'   => $registros
            );

        $banco = new Albuns();

        $banco->insert($data);
        
        $this->_redirect('/admin/albuns/msg/1');
    }


    public function editaalbunsAction(){
        $this->checasessao();

        $id = $this->getRequest()->getParam('id');
        
        $lista = new Admin();
        $result = $lista->Selectid('albuns',$id);

        $this->view->result = $result;
    }   

    public function acaoeditaalbunsAction(){
        $this->checasessao();

        $id = $this->getRequest()->getPost('id');

        $data = array(
                'nome'    => $this->getRequest()->getPost('nome')
            );

        $banco = new Albuns();

        $banco->update($data, 'id = '.$id);
        
        $this->_redirect('/admin/albuns/msg/2');
    }
    public function alteraalbunsAction(){
        $this->checasessao();

        $alteracao = $this->getRequest()->getParam('alteracao');
        $id = $this->getRequest()->getParam('id');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('albuns',$id);        

        foreach ($result as $campo => $valor) {
            $ordem = $valor['ordem'];
        }

        if ($alteracao == 'subir') {
            $novaordem = $ordem -1;
        }
        else{
            $novaordem = $ordem +1;
        }

        $result2 = $lista->Selectordem('albuns',$novaordem);

        foreach ($result2 as $campo => $valor) {
            $id_novo = $valor['id'];
        }

        $data = array(
            'ordem'        => $novaordem
        );
        $data2 = array(
            'ordem'        => $ordem
        );

        $banco = new Albuns();
         
        $banco->update($data, 'id = '.$id);

        $banco->update($data2, 'id = '.$id_novo);
        
        
        $result = $lista->Select('albuns');

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
    }

    public function deletaalbunsAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $lista = new Albuns();
        $result = $lista->Selectgaleria($id);        

        foreach ($result as $campo => $valor) { 
            unlink(UPLOAD_PATH . "/albuns/".$valor['caminho_img']);
            unlink(UPLOAD_PATH . "/albuns/tmb_".$valor['caminho_img']);
            
            $banco = new Imagemalbuns();
            $banco->delete('id = '.$valor['id']);
        }

        $banco = new Albuns();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/albuns/msg/3');
    }

    /*** Galeria Albuns***/

    public function albunsgaleriaAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        $id = $this->getRequest()->getParam('id');
        $this->view->id_galeria = $id;

        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');

        $lista = new Admin();
        $result1 = $lista->Selectid('albuns',$id);

        foreach ($result1 as $campo => $valor) {
            $this->view->nome_galeria = $valor['nome'];
        }

        $lista = new Imagemalbuns();
        $result = $lista->Selectgaleria($id);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->registros = count($result);
    }  

    public function adicionaalbunsgaleriaAction(){
        $this->checasessao();

        $this->view->id_galeria = $this->getRequest()->getParam('id_galeria');
    }  

    public function acaoadicionaalbunsgaleriaAction(){
        $this->checasessao();
        $erro == 0;

        $caminho = UPLOAD_PATH . "/albuns/";
        
        $upload = new Zend_File_Transfer_Adapter_Http();

        $upload->addValidator('FilesSize', false, array('max' => '12MB'))
                   ->addValidator('Extension', false, array('png','gif','jpeg','jpg'))       
                   ->setDestination($caminho);
        
        if (count($upload->getFileName()) != 0){

            $ext = strtolower(pathinfo($upload->getFileName(), PATHINFO_EXTENSION)); 

            $arquivo = md5(date(YmdHis)).md5('albuns').".".$ext;

            $nomearquivo = "tmp_".$arquivo;
            
            $upload->addFilter('Rename', $caminho.$nomearquivo,$upload->getFileName());
            
            if ($upload->receive()){}
            else {
                $erro = 5;
                foreach($upload -> getMessages() as $key=> $campo){
                   
                    switch ($key) {
                        case 'fileExtensionFalse':
                            $erro = 3;
                        break;
                        case 'fileUploadErrorIniSize':
                            $erro = 4;
                        break;
                    }
                }
            } 

            if ($erro == 0) {

                $caminho_imagem = $caminho.$nomearquivo;
                $caminho_imagem_novo = $caminho.$arquivo;
                $caminho_imagem_tmb = $caminho.'tmb_'.$arquivo;

                $image1 = Zend_Pdf_Image::imageWithPath($caminho_imagem);
                $largura_img = $image1->getPixelWidth();
                $altura_img = $image1->getPixelHeight();
                
                $w_tmb = 300;
                $h_tmb = 128;

                require_once('library/Thumb/Factory.php');
                $thumb = Php_Thumb_Factory::create($caminho_imagem);

                if($largura_img>=$altura_img){
                    $w = 900; 
                    $h = 600;
                    $h_novo = ceil(($w*$altura_img)/$largura_img);

                    if($h_novo > $h){
                        $thumb->adaptiveResize($w,$h); // com crop
                    }
                    else{
                        $thumb->resize($w,$h_novo); //sem crop
                    }
                }
                else{
                    $w = 900; 
                    $h = 600;  

                    $w_novo = ceil(($h*$largura_img)/$altura_img);

                    if($w_novo > $w){
                        $thumb->adaptiveResize($w,$h); // com crop
                    }
                    else{
                        $thumb->resize($w_novo,$h); //sem crop
                    }
                }

                $thumb->save($caminho_imagem_novo);

                $thumb->adaptiveResize($w_tmb,$h_tmb);
                $thumb->save($caminho_imagem_tmb);

                unlink($caminho_imagem);

                $lista = new Imagemalbuns();

                $result = $lista->Selectgaleria($this->getRequest()->getPost('id_galeria'));

                $registros = count($result)+1;

                $data = array(
                        'caminho_img'    => $arquivo,
                        'id_album'   => $this->getRequest()->getPost('id_galeria'),
                        'ordem'          => $registros
                    );

                $banco = new Imagemalbuns();

                $banco->insert($data);
                
                $this->_redirect('/admin/albunsgaleria/id/'.$this->getRequest()->getPost('id_galeria'));
            }

            else {
                $this->_redirect('/admin/albunsgaleria/erro/'.$erro);
            }
        }        
    }

    /*public function acaocropadicionaalbunsgaleriaAction(){
        $ratio = round(487/$this->getRequest()->getPost('w'),3);
        $targ_w = 487;
        $targ_h = ceil($this->getRequest()->getPost('h')*$ratio);
        $jpeg_quality = 90;

        $src =  UPLOAD_PATH . "/albuns/".$this->getRequest()->getPost('img');

        $novo_src = UPLOAD_PATH . "/albuns/".substr($this->getRequest()->getPost('img'), 4);
        $tbm_src = UPLOAD_PATH . "/albuns/tmb_".substr($this->getRequest()->getPost('img'), 4);

        $ext = strtolower(pathinfo($this->getRequest()->getPost('img'), PATHINFO_EXTENSION)); 

        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
        
        switch ($ext) {
            case 'jpg':
                $img_r = imagecreatefromjpeg($src);
            break;
            case 'gif':
                $img_r = imagecreatefromgif($src);
            break;
            case 'png':
                $img_r = imagecreatefrompng($src);
            break;
            case 'jpeg':
                $img_r = imagecreatefromjpeg($src);
            break;
        }

        imagecopyresampled($dst_r,$img_r,0,0,$this->getRequest()->getPost('x'),$this->getRequest()->getPost('y'),$targ_w,$targ_h,$this->getRequest()->getPost('w'),$this->getRequest()->getPost('h'));

        imagejpeg($dst_r,$novo_src,$jpeg_quality);


        switch ($ext) {
            case 'jpg':
                $img_r_n = imagecreatefromjpeg($novo_src);
            break;
            case 'gif':
                $img_r_n = imagecreatefromjpeg($novo_src);
            break;
            case 'png':
                $img_r_n = imagecreatefromjpeg($novo_src);
            break;
            case 'jpeg': 
                $img_r_n = imagecreatefromjpeg($novo_src);
            break;
        }

        $dst_r_tmb = ImageCreateTrueColor( 210, 235 );

        imagecopyresampled($dst_r_tmb,$img_r_n,0,0,0,54,210,317,487,730);
        imagejpeg($dst_r_tmb,$tbm_src,$jpeg_quality);

        unlink($src);


        $lista = new Imagemalbuns();
        $result = $lista->Selectgaleria($this->getRequest()->getPost('id_galeria'));

        $registros = count($result)+1;

        $data = array(
                'caminho_img'    => substr($this->getRequest()->getPost('img'), 4),
                'id_album'   => $this->getRequest()->getPost('id_galeria'),
                'ordem'          => $registros
            );

        $banco = new Imagemalbuns();

        $banco->insert($data);
        
        $this->_redirect('/admin/albunsgaleria/id/'.$this->getRequest()->getPost('id_galeria'));
    }*/
    public function alteraalbunsgaleriaAction(){
        $this->checasessao();

        $alteracao = $this->getRequest()->getParam('alteracao');
        $id = $this->getRequest()->getParam('id');
        $id_galeria = $this->getRequest()->getParam('id_galeria');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('imagens_albuns',$id);        

        foreach ($result as $campo => $valor) {
            $ordem = $valor['ordem'];
        }

        if ($alteracao == 'subir') {
            $novaordem = $ordem -1;
        }
        else{
            $novaordem = $ordem +1;
        }
        $lista = new Imagemalbuns();
        $result2 = $lista->Selectordem($novaordem,$id_galeria);

        foreach ($result2 as $campo => $valor) {
            $id_novo = $valor['id'];
        }

        $data = array(
            'ordem'        => $novaordem
        );
        $data2 = array(
            'ordem'        => $ordem
        );

        $banco = new Imagemalbuns();
         
        $banco->update($data, 'id = '.$id);

        $banco->update($data2, 'id = '.$id_novo);
        
        
        $result = $lista->Selectgaleria($id_galeria);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);
                  
        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
        $this->view->id_galeria = $id_galeria;
    }
    public function selecionaalbunsgaleriaAction(){
        $this->checasessao();

        $id = $this->getRequest()->getParam('id');
        $id_galeria = $this->getRequest()->getParam('id_galeria');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('imagens_albuns',$id);        

        
        $data = array(
            'capa'        => 0
        );
        $data2 = array(
            'capa'        => 1
        );

        $banco = new Imagemalbuns();
         
        $banco->update($data, array('capa = ?' => 1 , 'id_album = ?' => $id_galeria));

        $banco->update($data2, 'id = '.$id);
        
        $lista = new Imagemalbuns();
        $result = $lista->Selectgaleria($id_galeria);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);
                  
        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
        $this->view->id_galeria = $id_galeria;
    }

    public function deletaalbunsgaleriaAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $lista = new Admin();
        $result = $lista->Selectid('imagens_albuns',$id);        

        foreach ($result as $campo => $valor) {
            $caminho_img = $valor['caminho_img'];
            $id_galeria = $valor['id_album'];
            $ordem = $valor['ordem'];
        }
        unlink(UPLOAD_PATH . "/albuns/".$caminho_img);
        unlink(UPLOAD_PATH . "/albuns/tmb_".$caminho_img);

        $lista = new Imagemalbuns();
        $result = $lista->Selectdelete($ordem,$id_galeria);

        foreach ($result as $campo => $valor) {
            $data = array('ordem' => $valor['ordem']-1);

            $lista->update($data, 'id = '.$valor['id']);
        }

        $banco = new Imagemalbuns();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/albunsgaleria/id/'.$id_galeria.'/msg/3');
    }

    /*** Debutantes ***/

    public function debutantesAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        
        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');
        
        $lista = new Debutantes();
        $lista_capa = $lista->Selectcapa();

        $lista = new Admin();
        $result = $lista->Select('debutantes');

        $capa = '';
        $result_filtro = array();
        foreach ($result as $campo => $valor) {
            foreach ($lista_capa as $campo_capa => $valor_capa) {
                echo 1;
                if ($valor['id'] == $valor_capa['id']){
                    $capa = 1;
                }
            }
            $result_filtro[] = array(
                                    'id' => $valor["id"], 
                                    'nome' => $valor["nome"], 
                                    'ordem' => $valor["ordem"], 
                                    'capa' => $capa
                            );
            $capa ='';

        }

        $paginator = Zend_Paginator::factory($result_filtro);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->registros = count($result);
    }     

    public function adicionadebutantesAction(){
        $this->checasessao();
    }  

    public function acaoadicionadebutantesAction(){
        $this->checasessao();

        $lista = new Admin();
        $result = $lista->Select('debutantes');

        $registros = count($result)+1;

        $data = array(
                'nome'    => $this->getRequest()->getPost('nome'),
                'ordem'   => $registros
            );

        $banco = new Debutantes();

        $banco->insert($data);
        
        $this->_redirect('/admin/debutantes/msg/1');
    }


    public function editadebutantesAction(){
        $this->checasessao();

        $id = $this->getRequest()->getParam('id');
        
        $lista = new Admin();
        $result = $lista->Selectid('debutantes',$id);

        $this->view->result = $result;
    }   

    public function acaoeditadebutantesAction(){
        $this->checasessao();

        $id = $this->getRequest()->getPost('id');

        $data = array(
                'nome'    => $this->getRequest()->getPost('nome')
            );

        $banco = new Debutantes();

        $banco->update($data, 'id = '.$id);
        
        $this->_redirect('/admin/debutantes/msg/2');
    }
    public function alteradebutantesAction(){
        $this->checasessao();

        $alteracao = $this->getRequest()->getParam('alteracao');
        $id = $this->getRequest()->getParam('id');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('debutantes',$id);        

        foreach ($result as $campo => $valor) {
            $ordem = $valor['ordem'];
        }

        if ($alteracao == 'subir') {
            $novaordem = $ordem -1;
        }
        else{
            $novaordem = $ordem +1;
        }

        $result2 = $lista->Selectordem('debutantes',$novaordem);

        foreach ($result2 as $campo => $valor) {
            $id_novo = $valor['id'];
        }

        $data = array(
            'ordem'        => $novaordem
        );
        $data2 = array(
            'ordem'        => $ordem
        );

        $banco = new Debutantes();
         
        $banco->update($data, 'id = '.$id);

        $banco->update($data2, 'id = '.$id_novo);
        
        
        $result = $lista->Select('debutantes');

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
    }

    public function deletadebutantesAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $lista = new Debutantes();
        $result = $lista->Selectgaleria($id);        

        foreach ($result as $campo => $valor) { 
            unlink(UPLOAD_PATH . "/debutantes/".$valor['caminho_img']);
            unlink(UPLOAD_PATH . "/debutantes/tmb_".$valor['caminho_img']);
            
            $banco = new Imagemdebutantes();
            $banco->delete('id = '.$valor['id']);
        }

        $banco = new Debutantes();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/debutantes/msg/3');
    }

    /*** Galeria Albuns***/

    public function debutantesgaleriaAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        $id = $this->getRequest()->getParam('id');
        $this->view->id_galeria = $id;

        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');

        $lista = new Admin();
        $result1 = $lista->Selectid('debutantes',$id);

        foreach ($result1 as $campo => $valor) {
            $this->view->nome_galeria = $valor['nome'];
        }

        $lista = new Imagemdebutantes();
        $result = $lista->Selectgaleria($id);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->registros = count($result);
    }  

    public function adicionadebutantesgaleriaAction(){
        $this->checasessao();

        $this->view->id_galeria = $this->getRequest()->getParam('id_galeria');
    }  

    public function acaoadicionadebutantesgaleriaAction(){
        $this->checasessao();
        $erro == 0;

        $id_galeria = $this->getRequest()->getPost('id_galeria');

        $caminho = UPLOAD_PATH . "/debutantes/";
        
        $upload = new Zend_File_Transfer_Adapter_Http();

        $upload->addValidator('FilesSize', false, array('max' => '12MB'))
                   ->addValidator('Extension', false, array('png','gif','jpeg','jpg'))       
                   ->setDestination($caminho);
        
        if (count($upload->getFileName()) != 0){

            $ext = strtolower(pathinfo($upload->getFileName(), PATHINFO_EXTENSION)); 

            $arquivo =md5(date(YmdHis)).md5('debutantes').".".$ext;

            $nomearquivo = "tmp_".$arquivo;
            
            $upload->addFilter('Rename', $caminho.$nomearquivo,$upload->getFileName());
            
            if ($upload->receive()){}
            else {
                $erro = 5;
                foreach($upload -> getMessages() as $key=> $campo){
                   
                    switch ($key) {
                        case 'fileExtensionFalse':
                            $erro = 3;
                        break;
                        case 'fileUploadErrorIniSize':
                            $erro = 4;
                        break;
                    }
                }
            } 

            if ($erro == 0) {

                $caminho_imagem = $caminho.$nomearquivo;
                $caminho_imagem_novo = $caminho.$arquivo;
                $caminho_imagem_tmb = $caminho.'tmb_'.$arquivo;

                $image1 = Zend_Pdf_Image::imageWithPath($caminho_imagem);
                $largura_img = $image1->getPixelWidth();
                $altura_img = $image1->getPixelHeight();

                
                $w_tmb = 290;
                $h_tmb = 90;

                require_once('library/Thumb/Factory.php');
                $thumb = Php_Thumb_Factory::create($caminho_imagem);

                if($largura_img>=$altura_img){
                    $w = 900; 
                    $h = 600;
                    $h_novo = ceil(($w*$altura_img)/$largura_img);

                    if($h_novo > $h){
                        $thumb->adaptiveResize($w,$h); // com crop
                    }
                    else{
                        $thumb->resize($w,$h_novo); //sem crop
                    }
                }
                else{
                    $w = 900; 
                    $h = 600;  

                    $w_novo = ceil(($h*$largura_img)/$altura_img);

                    if($w_novo > $w){
                        $thumb->adaptiveResize($w,$h); // com crop
                    }
                    else{
                        $thumb->resize($w_novo,$h); //sem crop
                    }
                }

                $thumb->save($caminho_imagem_novo);


                $thumb->adaptiveResize($w_tmb,$h_tmb);
                $thumb->save($caminho_imagem_tmb);

                unlink($caminho_imagem);

                $lista = new Imagemdebutantes();
                $result = $lista->Selectgaleria($this->getRequest()->getPost('id_galeria'));

                $registros = count($result)+1;
                
                $data = array(
                    'caminho_img'    => $arquivo,
                    'id_debutantes'   => $this->getRequest()->getPost('id_galeria'),
                    'ordem'          => $registros
                );

                $banco = new Imagemdebutantes();

                $banco->insert($data);
                
                $this->_redirect('/admin/debutantesgaleria/id/'.$id_galeria.'/msg/1');
            }
            else{
                $this->_redirect('/admin/debutantesgaleria/id/'.$id_galeria.'/erro/'.$erro);
            }
        }        
    }
    public function alteradebutantesgaleriaAction(){
        $this->checasessao();

        $alteracao = $this->getRequest()->getParam('alteracao');
        $id = $this->getRequest()->getParam('id');
        $id_galeria = $this->getRequest()->getParam('id_galeria');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('imagens_debutantes',$id);        

        foreach ($result as $campo => $valor) {
            $ordem = $valor['ordem'];
        }

        if ($alteracao == 'subir') {
            $novaordem = $ordem -1;
        }
        else{
            $novaordem = $ordem +1;
        }
        $lista = new Imagemdebutantes();
        $result2 = $lista->Selectordem($novaordem,$id_galeria);

        foreach ($result2 as $campo => $valor) {
            $id_novo = $valor['id'];
        }

        $data = array(
            'ordem'        => $novaordem
        );
        $data2 = array(
            'ordem'        => $ordem
        );

        $banco = new Imagemdebutantes();
         
        $banco->update($data, 'id = '.$id);

        $banco->update($data2, 'id = '.$id_novo);
        
        
        $result = $lista->Selectgaleria($id_galeria);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);
                  
        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
        $this->view->id_galeria = $id_galeria;
    }
    public function selecionadebutantesgaleriaAction(){
        $this->checasessao();

        $id = $this->getRequest()->getParam('id');
        $id_galeria = $this->getRequest()->getParam('id_galeria');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('debutantes',$id);        

        
        $data = array(
            'capa'        => 0
        );
        $data2 = array(
            'capa'        => 1
        );

        $banco = new Imagemdebutantes();
         
        $banco->update($data, array('capa = ?' => 1 , 'id_debutantes = ?' => $id_galeria));

        $banco->update($data2, 'id = '.$id);
        
        $lista = new Imagemdebutantes();
        $result = $lista->Selectgaleria($id_galeria);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);
                  
        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
        $this->view->id_galeria = $id_galeria;
    }

    public function deletadebutantesgaleriaAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $lista = new Admin();
        $result = $lista->Selectid('imagens_debutantes',$id);        

        foreach ($result as $campo => $valor) {
            $caminho_img = $valor['caminho_img'];
            $id_galeria = $valor['id_debutantes'];
            $ordem = $valor['ordem'];
        }
        unlink(UPLOAD_PATH . "/debutantes/".$caminho_img);
        unlink(UPLOAD_PATH . "/debutantes/tmb_".$caminho_img);


        $lista = new Imagemdebutantes();
        $result = $lista->Selectdelete($ordem,$id_galeria);

        foreach ($result as $campo => $valor) {
            $data = array('ordem' => $valor['ordem']-1);

            $lista->update($data, 'id = '.$valor['id']);
        }
        
        $banco = new Imagemdebutantes();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/debutantesgaleria/id/'.$id_galeria.'/msg/3');
    }

    /*** Films ***/
  
    public function filmsAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        
        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');

        $lista = new Films();
        $result = $lista->Select('films');

        $result_casamento = $lista->Selecttipo(1);
        $result_debutante = $lista->Selecttipo(2);

        $result_filtro = array();

        foreach ($result as $campo => $valor) {
            switch ($valor['tipo']) {
                case 1:
                    $tipo = 'casamento';
                break;
                case 2:
                    $tipo = 'debutante';
                break;
            }
            $result_filtro[] = array('nome' => $valor['nome'],
                                     'caminho_video' => $valor['caminho_video'],
                                     'id' => $valor['id'],
                                     'tipo' => $tipo,
                                     'ordem' => $valor['ordem']
                                     );    
        }

        $paginator = Zend_Paginator::factory($result_filtro);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;

        $this->view->casamentos = count($result_casamento);
        $this->view->debutantes = count($result_debutante);
    } 
    public function adicionafilmsAction(){
        $this->checasessao();
    } 
    public function acaoadicionafilmsAction(){
        $this->checasessao();

        $lista = new Films();
        $result = $lista->Selecttipo($this->getRequest()->getPost('tipo'));

        $registros = count($result)+1;

        $caminho_video = $this->getRequest()->getPost('video');
        // $caminho_video = str_replace('http://www.youtube.com/watch?v=','',$caminho_video);
        // $caminho_video = str_replace('https://www.youtube.com/watch?v=','',$caminho_video);
        // $caminho_video = str_replace('http://youtu.be/','',$caminho_video);

        $video = explode('?',$caminho_video);

        $data = array(
                'nome'    => $this->getRequest()->getPost('nome'),
                // 'caminho_video'    => $video[0],
                'caminho_video'    => $caminho_video,
                'tipo'    => $this->getRequest()->getPost('tipo'),
                'ordem'   => $registros
            );

        $banco = new Films();

        $banco->insert($data);
        
        $this->_redirect('/admin/films/msg/1');
    }


    public function editafilmsAction(){
        $this->checasessao();

        $id = $this->getRequest()->getParam('id');
        
        $lista = new Admin();
        $result = $lista->Selectid('films',$id);

        $this->view->result = $result;
    }  

    public function acaoeditafilmsAction(){
        $this->checasessao();

        $id = $this->getRequest()->getPost('id');

        $lista = new Admin();
        $result = $lista->Selectid('films',$id);

        $caminho_video = $this->getRequest()->getPost('video');
        // $caminho_video = str_replace('http://www.youtube.com/watch?v=','',$caminho_video);
        // $caminho_video = str_replace('https://www.youtube.com/watch?v=','',$caminho_video);
        // $caminho_video = str_replace('http://youtu.be/','',$caminho_video);

        $video = explode('?',$caminho_video);

        foreach ($result as $campo => $valor) {
            if ($this->getRequest()->getPost('tipo') != $valor['tipo']){
        
                $lista = new Films();
                $result_casamento = $lista->Selecttipo($this->getRequest()->getPost('tipo'));
        
                $ordem = count($result_casamento)+1;
            }
            else{
                $ordem = $valor['ordem'];
            }
        }

        $data = array(
                'nome'              => $this->getRequest()->getPost('nome'),
                // 'caminho_video'     => $video[0],
                'caminho_video'     => $caminho_video,
                'tipo'              => $this->getRequest()->getPost('tipo'),
                'ordem'             => $ordem
            );

        $banco = new Films();

        $banco->update($data, 'id = '.$id);
        
        $this->_redirect('/admin/films/msg/2');
    }

    public function deletafilmsAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $banco = new Films();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/films/msg/3');
    }

    public function alterafilmsAction(){
        $this->checasessao();

        $alteracao = $this->getRequest()->getParam('alteracao');
        $id = $this->getRequest()->getParam('id');
        $pagina = $this->getRequest()->getParam('pagina');
        $tipo = $this->getRequest()->getParam('tipo');

        switch ($tipo) {   
            case "casamento":
                $tp = 1;
            break;
            case "debutante":
                $tp = 2;                
            break;
        }

        $lista = new Admin();
        $result = $lista->Selectid('films',$id); 

        foreach ($result as $campo => $valor) {
            $ordem = $valor['ordem'];
        }

        if ($alteracao == 'subir') {
            $novaordem = $ordem -1;
        }
        else{
            $novaordem = $ordem +1;
        }

        $lista2 = new Films();
        $result2 = $lista2->Selectordem($novaordem,$tp);

        foreach ($result2 as $campo => $valor) {
            $id_novo = $valor['id'];
        }

        $data = array(
            'ordem'        => $novaordem
        );
        $data2 = array(
            'ordem'        => $ordem
        );

        $banco = new Films();
         
        $banco->update($data, 'id = '.$id);

        $banco->update($data2, 'id = '.$id_novo);
        
        
        
        $lista = new Films();
        $result = $lista->Select('films');

        $result_casamento = $lista->Selecttipo(1);
        $result_debutante = $lista->Selecttipo(2);

        $result_filtro = array();

        foreach ($result as $campo => $valor) {
            switch ($valor['tipo']) {
                case 1:
                    $tipo = 'casamento';
                break;
                case 2:
                    $tipo = 'debutante';
                break;
            }
            $result_filtro[] = array('nome' => $valor['nome'],
                                     'caminho_video' => $valor['caminho_video'],
                                     'id' => $valor['id'],
                                     'tipo' => $tipo,
                                     'ordem' => $valor['ordem']
                                     );    
        }

        $paginator = Zend_Paginator::factory($result_filtro);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->pagina = $pagina;

        $this->view->casamentos = count($result_casamento);
        $this->view->debutantes = count($result_debutante);
    }

    /*** Prewedding ***/
  
    public function preweddingAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        
        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');

        $lista = new Prewedding();
        $result = $lista->Select('prewedding');

        $result_video = $lista->Selecttipo(1);
        $result_foto = $lista->Selecttipo(0);

        $result_filtro = array();

        foreach ($result as $campo => $valor) {
            switch ($valor['tipo']) {
                case 1:
                    $tipo = 'video';
                break;
                case 0:
                    $tipo = 'foto';
                break;
            }
            $result_filtro[] = array('nome' => $valor['nome'],
                                     'caminho_video' => $valor['caminho_video'],
                                     'caminho_imagem' => $valor['caminho_imagem'],
                                     'id' => $valor['id'],
                                     'tipo' => $tipo,
                                     'ordem' => $valor['ordem']
                                     );    
        }

        $paginator = Zend_Paginator::factory($result_filtro);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;

        $this->view->videos = count($result_video);
        $this->view->fotos = count($result_foto);
    } 
    public function adicionapreweddingAction(){
        $this->checasessao();
    } 
    public function acaoadicionapreweddingAction(){
        $this->checasessao();
        $erro == 0;
        $caminho_imagem_novo = null;
        $nome_imagem_novo = null;

        $caminho = UPLOAD_PATH . "/prewedding/";
        
        $upload = new Zend_File_Transfer_Adapter_Http();

        $upload->addValidator('FilesSize', false, array('max' => '12MB'))
                   // ->addValidator('Extension', false, array('png','gif','jpeg','jpg',))       
                   ->addValidator('Extension', false, 'png,gif,jpeg,jpg')
                   ->setDestination($caminho);
        
        if (count($upload->getFileName()) != 0){

            $ext = strtolower(pathinfo($upload->getFileName(), PATHINFO_EXTENSION)); 

            $arquivo = md5(date(YmdHis)).md5('prewedding').'.'.$ext;

            $nomearquivo = "tmp_".$arquivo;
            
            $upload->addFilter('Rename', $caminho.$nomearquivo,$upload->getFileName());
            
            if ($upload->receive()){}
            else {
                $erro = 5;
                foreach($upload -> getMessages() as $key=> $campo){
                   
                    switch ($key) {
                        case 'fileExtensionFalse':
                            $erro = 3;
                        break;
                        case 'fileUploadErrorIniSize':
                            $erro = 4;
                        break;
                    }
                }
            } 
            if ($erro == 0) {

                $caminho_imagem = $caminho.$nomearquivo;

                // $image1 = Zend_Pdf_Image::imageWithPath($caminho_imagem);
                // $largura_img = $image1->getPixelWidth();
                // $altura_img = $image1->getPixelHeight();

                $nome_imagem_novo = md5(date(YmdHis)).md5('home').'.'.$ext;
                $caminho_imagem_novo = $caminho.$nome_imagem_novo;
                
                $w = 1024; $h = 480;
                
                require_once('library/Thumb/Factory.php');
                $thumb = Php_Thumb_Factory::create($caminho_imagem);

                // resize com crop
                $thumb->adaptiveResize($w,$h);
                // resize sem crop
                // $thumb->resize($w,$h);

                $thumb->save($caminho_imagem_novo);

                unlink($caminho_imagem);   
            }
        } 

        if($erro > 0) return $this->_redirect('/admin/prewedding/erro/'.$erro);

        $lista = new Prewedding();
        $result = $lista->Selecttipo($this->getRequest()->getPost('tipo'));

        $registros = count($result)+1;

        $caminho_video = $this->getRequest()->getPost('video');
        // $caminho_video = str_replace('http://www.youtube.com/watch?v=','',$caminho_video);
        // $caminho_video = str_replace('https://www.youtube.com/watch?v=','',$caminho_video);
        // $caminho_video = str_replace('http://youtu.be/','',$caminho_video);

        $video = explode('?',$caminho_video);
        $tipo = $this->getRequest()->getPost('tipo');

        $data = array(
                'nome'    => $this->getRequest()->getPost('nome'),
                // 'caminho_video'    => $tipo==1 ? $video[0] : null,
                'caminho_video'    => $tipo==1 ? $caminho_video : null,
                'caminho_imagem'   => $tipo==0 ? $nome_imagem_novo : null,
                'tipo'    => $this->getRequest()->getPost('tipo'),
                'ordem'   => $registros
            );

        $banco = new Prewedding();

        $banco->insert($data);
        
        $this->_redirect('/admin/prewedding/msg/1');
    }


    public function editapreweddingAction(){
        $this->checasessao();

        $id = $this->getRequest()->getParam('id');
        
        $lista = new Admin();
        $result = $lista->Selectid('prewedding',$id);

        $this->view->result = $result;
    }  

    public function acaoeditapreweddingAction(){
        $this->checasessao();
        $erro == 0;
        $caminho_imagem_novo = null;
        $nome_imagem_novo = null;

        $caminho = UPLOAD_PATH . "/prewedding/";
        
        $upload = new Zend_File_Transfer_Adapter_Http();

        $upload->addValidator('FilesSize', false, array('max' => '12MB'))
                   // ->addValidator('Extension', false, array('png','gif','jpeg','jpg',))       
                   ->addValidator('Extension', false, 'png,gif,jpeg,jpg')
                   ->setDestination($caminho);
        
        if (count($upload->getFileName()) != 0){

            $ext = strtolower(pathinfo($upload->getFileName(), PATHINFO_EXTENSION)); 

            $arquivo = md5(date(YmdHis)).md5('prewedding').'.'.$ext;

            $nomearquivo = "tmp_".$arquivo;
            
            $upload->addFilter('Rename', $caminho.$nomearquivo,$upload->getFileName());
            
            if ($upload->receive()){}
            else {
                $erro = 5;
                foreach($upload -> getMessages() as $key=> $campo){
                   
                    switch ($key) {
                        case 'fileExtensionFalse':
                            $erro = 3;
                        break;
                        case 'fileUploadErrorIniSize':
                            $erro = 4;
                        break;
                    }
                }
            } 
            if ($erro == 0) {

                $caminho_imagem = $caminho.$nomearquivo;

                // $image1 = Zend_Pdf_Image::imageWithPath($caminho_imagem);
                // $largura_img = $image1->getPixelWidth();
                // $altura_img = $image1->getPixelHeight();

                $nome_imagem_novo = md5(date(YmdHis)).md5('home').'.'.$ext;
                $caminho_imagem_novo = $caminho.$nome_imagem_novo;
                
                $w = 1024; $h = 480;
                
                require_once('library/Thumb/Factory.php');
                $thumb = Php_Thumb_Factory::create($caminho_imagem);

                // resize com crop
                $thumb->adaptiveResize($w,$h);
                // resize sem crop
                // $thumb->resize($w,$h);

                $thumb->save($caminho_imagem_novo);

                unlink($caminho_imagem);   
            }
        } 

        if($erro > 0) return $this->_redirect('/admin/prewedding/erro/'.$erro);

        $id = $this->getRequest()->getPost('id');

        $lista = new Admin();
        $result = $lista->Selectid('prewedding',$id);

        $caminho_video = $this->getRequest()->getPost('video');
        // $caminho_video = str_replace('http://www.youtube.com/watch?v=','',$caminho_video);
        // $caminho_video = str_replace('https://www.youtube.com/watch?v=','',$caminho_video);
        // $caminho_video = str_replace('http://youtu.be/','',$caminho_video);

        $video = explode('?',$caminho_video);
        $tipo = $this->getRequest()->getPost('tipo');

        foreach ($result as $campo => $valor) {
            if ($this->getRequest()->getPost('tipo') != $valor['tipo']){
        
                $lista = new Prewedding();
                $result_tipo = $lista->Selecttipo($this->getRequest()->getPost('tipo'));
        
                $ordem = count($result_tipo)+1;
            }
            else{
                $ordem = $valor['ordem'];
            }
        }

        if($tipo==0 && $caminho_imagem_novo) unlink($caminho.$result[0]['caminho_imagem']);

        $data = array(
                'nome'              => $this->getRequest()->getPost('nome'),
                // 'caminho_video'     => $tipo==1 ? $video[0] : null,
                'caminho_video'     => $tipo==1 ? $caminho_video : null,
                'caminho_imagem'    => $tipo==0 ? $nome_imagem_novo : null,
                'tipo'              => $this->getRequest()->getPost('tipo'),
                'ordem'             => $ordem
            );

        $banco = new Prewedding();

        $banco->update($data, 'id = '.$id);
        
        $this->_redirect('/admin/prewedding/msg/2');
    }

    public function deletapreweddingAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $banco = new Prewedding();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/prewedding/msg/3');
    }

    public function alterapreweddingAction(){
        $this->checasessao();

        $alteracao = $this->getRequest()->getParam('alteracao');
        $id = $this->getRequest()->getParam('id');
        $pagina = $this->getRequest()->getParam('pagina');
        $tipo = $this->getRequest()->getParam('tipo');

        switch ($tipo) {   
            case "video":
                $tp = 1;
            break;
            case "foto":
                $tp = 0;                
            break;
        }

        $lista = new Admin();
        $result = $lista->Selectid('prewedding',$id); 

        foreach ($result as $campo => $valor) {
            $ordem = $valor['ordem'];
        }

        if ($alteracao == 'subir') {
            $novaordem = $ordem -1;
        }
        else{
            $novaordem = $ordem +1;
        }

        $lista2 = new Prewedding();
        $result2 = $lista2->Selectordem($novaordem,$tp);

        foreach ($result2 as $campo => $valor) {
            $id_novo = $valor['id'];
        }

        $data = array(
            'ordem'        => $novaordem
        );
        $data2 = array(
            'ordem'        => $ordem
        );

        $banco = new Prewedding();
         
        $banco->update($data, 'id = '.$id);

        $banco->update($data2, 'id = '.$id_novo);
        
        
        
        $lista = new Prewedding();
        $result = $lista->Select('prewedding');

        $result_video = $lista->Selecttipo(1);
        $result_foto = $lista->Selecttipo(0);

        $result_filtro = array();

        foreach ($result as $campo => $valor) {
            switch ($valor['tipo']) {
                case 1:
                    $tipo = 'video';
                break;
                case 0:
                    $tipo = 'foto';
                break;
            }
            $result_filtro[] = array('nome' => $valor['nome'],
                                     'caminho_video' => $valor['caminho_video'],
                                     'caminho_imagem' => $valor['caminho_imagem'],
                                     'id' => $valor['id'],
                                     'tipo' => $tipo,
                                     'ordem' => $valor['ordem']
                                     );    
        }

        $paginator = Zend_Paginator::factory($result_filtro);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->pagina = $pagina;

        $this->view->videos = count($result_video);
        $this->view->fotos = count($result_foto);
    }

    /*** Galeria Prewedding ***/

    public function preweddinggaleriaAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        $id = $this->getRequest()->getParam('id');
        $this->view->id_galeria = $id;

        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');

        $lista = new Admin();
        $result1 = $lista->Selectid('prewedding',$id);

        foreach ($result1 as $campo => $valor) {
            $this->view->nome_galeria = $valor['nome'];
        }

        $lista = new Imagemprewedding();
        $result = $lista->Selectgaleria($id);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->registros = count($result);
    }  

    public function adicionapreweddinggaleriaAction(){
        $this->checasessao();

        $this->view->id_galeria = $this->getRequest()->getParam('id_galeria');
    }  

    public function acaoadicionapreweddinggaleriaAction(){
        $this->checasessao();
        $erro == 0;

        $caminho = UPLOAD_PATH . "/prewedding/";
        
        $upload = new Zend_File_Transfer_Adapter_Http();

        $upload->addValidator('FilesSize', false, array('max' => '12MB'))
                   ->addValidator('Extension', false, array('png','gif','jpeg','jpg'))       
                   ->setDestination($caminho);
        
        if (count($upload->getFileName()) != 0){

            $ext = strtolower(pathinfo($upload->getFileName(), PATHINFO_EXTENSION)); 

            $arquivo = md5(date(YmdHis)).md5('prewedding').".".$ext;

            $nomearquivo = "tmp_".$arquivo;
            
            $upload->addFilter('Rename', $caminho.$nomearquivo,$upload->getFileName());
            
            if ($upload->receive()){}
            else {
                $erro = 5;
                foreach($upload -> getMessages() as $key=> $campo){
                   
                    switch ($key) {
                        case 'fileExtensionFalse':
                            $erro = 3;
                        break;
                        case 'fileUploadErrorIniSize':
                            $erro = 4;
                        break;
                    }
                }
            } 

            if ($erro == 0) {

                $caminho_imagem = $caminho.$nomearquivo;
                $caminho_imagem_novo = $caminho.$arquivo;
                $caminho_imagem_tmb = $caminho.'tmb_'.$arquivo;

                $image1 = Zend_Pdf_Image::imageWithPath($caminho_imagem);
                $largura_img = $image1->getPixelWidth();
                $altura_img = $image1->getPixelHeight();
                
                $w_tmb = 300;
                $h_tmb = 128;

                require_once('library/Thumb/Factory.php');
                $thumb = Php_Thumb_Factory::create($caminho_imagem);

                if($largura_img>=$altura_img){
                    $w = 900; 
                    $h = 600;
                    $h_novo = ceil(($w*$altura_img)/$largura_img);

                    if($h_novo > $h){
                        $thumb->adaptiveResize($w,$h); // com crop
                    }
                    else{
                        $thumb->resize($w,$h_novo); //sem crop
                    }
                }
                else{
                    $w = 900; 
                    $h = 600;  

                    $w_novo = ceil(($h*$largura_img)/$altura_img);

                    if($w_novo > $w){
                        $thumb->adaptiveResize($w,$h); // com crop
                    }
                    else{
                        $thumb->resize($w_novo,$h); //sem crop
                    }
                }

                $thumb->save($caminho_imagem_novo);

                $thumb->adaptiveResize($w_tmb,$h_tmb);
                $thumb->save($caminho_imagem_tmb);

                unlink($caminho_imagem);

                $lista = new Imagemprewedding();

                $result = $lista->Selectgaleria($this->getRequest()->getPost('id_galeria'));

                $registros = count($result)+1;

                $data = array(
                        'caminho_img'    => $arquivo,
                        'id_prewedding'  => $this->getRequest()->getPost('id_galeria'),
                        'ordem'          => $registros
                    );

                $banco = new Imagemprewedding();

                $banco->insert($data);
                
                $this->_redirect('/admin/preweddinggaleria/id/'.$this->getRequest()->getPost('id_galeria'));
            }

            else {
                $this->_redirect('/admin/preweddinggaleria/erro/'.$erro);
            }
        }        
    }

    public function alterapreweddinggaleriaAction(){
        $this->checasessao();

        $alteracao = $this->getRequest()->getParam('alteracao');
        $id = $this->getRequest()->getParam('id');
        $id_galeria = $this->getRequest()->getParam('id_galeria');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('imagens_prewedding',$id);        

        foreach ($result as $campo => $valor) {
            $ordem = $valor['ordem'];
        }

        if ($alteracao == 'subir') {
            $novaordem = $ordem -1;
        }
        else{
            $novaordem = $ordem +1;
        }
        $lista = new Imagemprewedding();
        $result2 = $lista->Selectordem($novaordem,$id_galeria);

        foreach ($result2 as $campo => $valor) {
            $id_novo = $valor['id'];
        }

        $data = array(
            'ordem'        => $novaordem
        );
        $data2 = array(
            'ordem'        => $ordem
        );

        $banco = new Imagemprewedding();
         
        $banco->update($data, 'id = '.$id);

        $banco->update($data2, 'id = '.$id_novo);
        
        
        $result = $lista->Selectgaleria($id_galeria);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);
                  
        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
        $this->view->id_galeria = $id_galeria;
    }
    public function selecionapreweddinggaleriaAction(){
        $this->checasessao();

        $id = $this->getRequest()->getParam('id');
        $id_galeria = $this->getRequest()->getParam('id_galeria');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('imagens_prewedding',$id);        
        $img_capa = $result[0]['caminho_img'];
        
        $data = array(
            'capa'        => 0
        );
        $data2 = array(
            'capa'        => 1
        );

        $banco = new Imagemprewedding();
         
        $banco->update($data, array('capa = ?' => 1 , 'id_prewedding = ?' => $id_galeria));
        $banco->update($data2, 'id = '.$id);

        $banco2 = new Prewedding();
        $banco2->update(array('caminho_imagem'=>$img_capa),'id="'.$id_galeria.'"');
        
        $lista = new Imagemprewedding();
        $result = $lista->Selectgaleria($id_galeria);

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);
                  
        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
        $this->view->id_galeria = $id_galeria;
    }

    public function deletapreweddinggaleriaAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $lista = new Admin();
        $result = $lista->Selectid('imagens_prewedding',$id);        

        foreach ($result as $campo => $valor) {
            $caminho_img = $valor['caminho_img'];
            $id_galeria = $valor['id_prewedding'];
            $ordem = $valor['ordem'];
        }
        unlink(UPLOAD_PATH . "/prewedding/".$caminho_img);
        unlink(UPLOAD_PATH . "/prewedding/tmb_".$caminho_img);

        $lista = new Imagemprewedding();
        $result = $lista->Selectdelete($ordem,$id_galeria);

        foreach ($result as $campo => $valor) {
            $data = array('ordem' => $valor['ordem']-1);

            $lista->update($data, 'id = '.$valor['id']);
        }

        $banco = new Imagemprewedding();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/preweddinggaleria/id/'.$id_galeria.'/msg/3');
    }

    /*** Depoimentos ***/
   
    public function depoimentosAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        
        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');

        $lista = new Depoimentos();
        $result = $lista->Select();

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->registros = count($result);
    } 
    public function adicionadepoimentosAction(){
        $this->checasessao();
    } 
    public function acaoadicionadepoimentosAction(){
        $this->checasessao();

        $lista = new Depoimentos();
        $result = $lista->Select();

        $registros = count($result)+1;

        $data = array(
                'texto'    => $this->getRequest()->getPost('texto'),
                'assinatura'    => $this->getRequest()->getPost('assinatura'),
                'ordem'   => $registros
            );

        $banco = new Depoimentos();

        $banco->insert($data);
        
        $this->_redirect('/admin/depoimentos/msg/1');
    }


    public function editadepoimentosAction(){
        $this->checasessao();

        $id = $this->getRequest()->getParam('id');
        
        $lista = new Admin();
        $result = $lista->Selectid('depoimentos',$id);

        $this->view->result = $result;
    }  

    public function acaoeditadepoimentosAction(){
        $this->checasessao();

        $id = $this->getRequest()->getPost('id');

        $data = array(
                'texto'              => $this->getRequest()->getPost('texto'),
                'assinatura'     => $this->getRequest()->getPost('assinatura')
            );

        $banco = new Depoimentos();

        $banco->update($data, 'id = '.$id);
        
        $this->_redirect('/admin/depoimentos/msg/2');
    }
    public function deletadepoimentosAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $banco = new Depoimentos();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/depoimentos/msg/3');
    }
    public function alteradepoimentosAction(){
        $this->checasessao();

        $alteracao = $this->getRequest()->getParam('alteracao');
        $id = $this->getRequest()->getParam('id');
        $pagina = $this->getRequest()->getParam('pagina');

        $lista = new Admin();
        $result = $lista->Selectid('depoimentos',$id);        

        foreach ($result as $campo => $valor) {
            $ordem = $valor['ordem'];
        }

        if ($alteracao == 'subir') {
            $novaordem = $ordem -1;
        }
        else{
            $novaordem = $ordem +1;
        }

        $result2 = $lista->Selectordem('depoimentos',$novaordem);

        foreach ($result2 as $campo => $valor) {
            $id_novo = $valor['id'];
        }

        $data = array(
            'ordem'        => $novaordem
        );
        $data2 = array(
            'ordem'        => $ordem
        );

        $banco = new Depoimentos();
         
        $banco->update($data, 'id = '.$id);

        $banco->update($data2, 'id = '.$id_novo);
        
        
        $result = $lista->Select('depoimentos');

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->pagina = $pagina;
        $this->view->paginator = $paginator;
        $this->view->registros = count($result);
    }
 
    /*** usuario ***/

    public function contatoAction(){
        $this->checasessao();

        $lista = new Contato();
        $result = $lista->Select();

        $this->view->result = $result;
    }  

    public function acaocontatoAction(){
        $this->checasessao();
        
        $erro = 0;

        $id = $this->getRequest()->getPost('id');
        $caminho_imagem_antiga = $this->getRequest()->getPost('caminho_imagem');

        $caminho = UPLOAD_PATH . "/contato/";
        
        $upload = new Zend_File_Transfer_Adapter_Http();

        $upload->addValidator('FilesSize', false, array('max' => '12MB'))
                   ->addValidator('Extension', false, array('png','gif','jpeg','jpg'))       
                   ->setDestination($caminho);
        
        if (count($upload->getFileName()) != 0){

            $ext = strtolower(pathinfo($upload->getFileName(), PATHINFO_EXTENSION)); 

            $arquivo =md5(date(YmdHis)).md5('contato').".".$ext;

            $nomearquivo = "tmp_".$arquivo;
            
            $upload->addFilter('Rename', $caminho.$nomearquivo,$upload->getFileName());
            
            if ($upload->receive()){}
            else {
                $erro = 5;
                foreach($upload -> getMessages() as $key=> $campo){
                   
                    switch ($key) {
                        case 'fileExtensionFalse':
                            $erro = 3;
                        break;
                        case 'fileUploadErrorIniSize':
                            $erro = 4;
                        break;
                    }
                }
            } 

            if ($erro == 0) {

                $caminho_imagem = $caminho.$nomearquivo;
                $caminho_imagem_novo = $caminho.$arquivo;
                $caminho_imagem_tmb = $caminho.'tmb_'.$arquivo;
            
                $w = 370; 
                $h = 220;
                
                require_once('library/Thumb/Factory.php');
                
                $thumb = Php_Thumb_Factory::create($caminho_imagem);
                
                $thumb->adaptiveResize($w,$h); // com crop
                
                $thumb->save($caminho_imagem_novo);

                unlink($caminho_imagem);
                unlink($caminho.$caminho_imagem_antiga);

                $lista = new Contato();

                $data = array('caminho_imagem'    => $arquivo);

                $lista->update($data, 'id = '.$id);
                
                $this->_redirect('/admin/contato/msg/1');
            }
            else{
                $this->_redirect('/admin/contato/erro/'.$erro);
            }
        }  
    } 

    /*** usuario ***/

    public function usuariosAction(){
        $this->checasessao();

        $pagina = $this->getRequest()->getParam('pagina');
        $erro = $this->getRequest()->getParam('erro');
        
        if ($pagina == ''){
            $this->view->pagina = 1;
        }
        else{
            $this->view->pagina = $pagina;   
        }

        $this->view->msg = $this->getRequest()->getParam('msg');

        $lista = new Usuario();
        $result = $lista->Select();

        $paginator = Zend_Paginator::factory($result);
        $paginator->setCurrentPageNumber($pagina)
                  ->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
        $this->view->erro = $erro;
        $this->view->registros = count($result);
    } 
    public function adicionausuarioAction(){
        $this->checasessao();
    } 
    public function acaoadicionausuarioAction(){
        $this->checasessao();

        $data = array(
                'usuario'    => $this->getRequest()->getPost('usuario'),
                'senha'    => md5($this->getRequest()->getPost('senha'))
            );

        $banco = new Usuario();

        $banco->insert($data);
        
        $this->_redirect('/admin/usuarios/msg/1');
    }
    public function deletausuarioAction(){
        $this->checasessao();

        $id =$this->getRequest()->getParam('id');

        $banco = new Usuario();

        $banco->delete('id = '.$id);
        
        $this->_redirect('/admin/usuarios/msg/3');
    }
    
	public function logoutAction(){
        Zend_Session::destroy(true);
        $this->_redirect('/admin/?e=4');
    }

    private function checasessao(){
        $sessao = new Zend_Session_Namespace('Zend_Auth');
        if($sessao->usuario == ""){
            $this->_redirect('/admin/?e=3');  
        }
    }



}
?>