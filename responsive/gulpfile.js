var gulp        = require('gulp'),
    plumber     = require('gulp-plumber'),
    stylus      = require('gulp-stylus'),
    rupture     = require('rupture'),
    koutoSwiss  = require('kouto-swiss'),
    browserSync = require('browser-sync');

gulp.task('stylus', function() {
    return gulp.src('./styl/responsive.styl')
        .pipe(plumber())
        .pipe(stylus({
            use: [koutoSwiss(), rupture()],
            compress: true
        }))
        .pipe(gulp.dest('../public/css/'))
        .pipe(browserSync.reload({ stream: true }))
});

gulp.task('browserSync', function() {
    browserSync({
        proxy: 'studiok.dev',
        open: false,
        notify: false
    });
});

gulp.task('watch', function() {
    gulp.watch('./styl/**/*.styl', ['stylus']);
});

gulp.task('default', ['stylus', 'watch', 'browserSync']);
