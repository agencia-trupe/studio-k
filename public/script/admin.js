$(function(){
	$(".close-green").click(function(){
		$("#message-green").css({'display':'none'});
	});
	
	$(".close-red").click(function(){
		$("#message-red").css({'display':'none'});
	});

	$("#editanoticia_altera_imagem").click(function(){
		$("#tr_upload").css({'display':'block'});
	});
	$("#editanoticia_visualizar_imagem").click(function(){
		$('#visualizar').css({"display":"block"});
		$('#visualizar').css({"height":$('#visualizar img').height()+"px",
							  "margin-top":"-"+$('#visualizar img').height()/2+"px"});
	    $('#fundo_ligthbox').css({"display":"block"});
	});
	$("#fechar_visualizar").click(function(){
		$('#visualizar').css({"display":"none"});
		$('#fundo_ligthbox').css({"display":"none"});
	});
});