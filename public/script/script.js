$(document).ready(function(e){
	$('.contato_cidade, .bt_contato').on('click',function(e){
		e.preventDefault();

		$('.bt_contato').removeClass('active');
		$(this).addClass('active');

		var formId = '#formulario_contato_'+this.id.split('_').slice(-1)[0];

		// FBpixel
		if(APPLICATION_ENV=='production') {
			switch(formId) {
				case '#formulario_contato_abc': // ABC
					fbq('init', '243508816075974'); break;
				case '#formulario_contato_id': // CPS
					fbq('init', '664018790450055'); break;
				default: // contato
					fbq('init', '282888248781325'); 
			}
			fbq('track', 'PageView');
		}

		$('.formulario_contato').slideUp(function(){
			$(formId).slideDown();
		});

		return false;
	});
});

function validacontato(form){
	var erro = "";
	// var form = document.formulario_contato;

	if(form.nome.value == "" || form.nome.value == "Nome"){
		erro += "Campo nome nao pode ser vazio<br>";
		form.nome.style.backgroundColor="#DA2128";
	}
    var filtro_mail = /^.+@.+\..{2,3}$/

	if (!filtro_mail.test(form.email.value)) {
		erro += "Campo email invalido<br>";
		form.email.style.backgroundColor="#DA2128";
	}
	if(form.email.value == "E-mail"|| form.nome.value == "E-mail"){
		erro += "Campo email nao pode ser vazio<br>";
		form.email.style.backgroundColor="#DA2128";
	}
	if(form.telefone.value == "" || form.telefone.value == "Telefone"){
		erro += "Campo telefone nao pode ser vazio<br>";
		form.telefone.style.backgroundColor="#DA2128";
	}
	if(form.data_evento.value == "" || form.data_evento.value == "Data do evento"){
		erro += "Campo data do evento nao pode ser vazio<br>";
		form.data_evento.style.backgroundColor="#DA2128";
	}
	if(form.telefone.value.length < 13){
		erro += "Campo telefone invalido<br>";
		form.telefone.style.backgroundColor="#DA2128";
	}
	if(form.mensagem.value == "" || form.mensagem.value == "Mensagem"){
		erro += "Campo mensagem nao pode ser vazio<br>";
		form.mensagem.style.backgroundColor="#DA2128";
	}

	if(erro != ""){
		lightbox('lightbox',erro);
		return false;
	}
}

function mensagemcontato(msg){
	lightbox('lightbox',msg);
}

function lightbox(nome_div,msg){

		if (document.getElementById(nome_div).style.display=="block"){
			document.getElementById(nome_div).style.display="none";
			document.getElementById('fundo_ligthbox').style.display="none";

		}
		else{
  			html = msg;
  			html += "<a href=javascript:; onClick=lightbox('lightbox','');>FECHAR</a>";
			document.getElementById(nome_div).innerHTML = html;
			document.getElementById(nome_div).style.display="block";
			document.getElementById('fundo_ligthbox').style.display="block";
		}
}

function Mascaratelefone(objeto){
   if(objeto.value.length == 0)
     objeto.value = '(' + objeto.value;

   if(objeto.value.length == 3)
      objeto.value = objeto.value + ')';

}

function Mascaradata(objeto){
   	if(objeto.value.length == 2)
      objeto.value = objeto.value + '/';

 	if(objeto.value.length == 5)
     objeto.value = objeto.value + '/';
}
function mostracasamentos(){
	$('#menu_films_casamento').css({"display":"block"});
	$('#link_mostra_casamento').addClass("sel");

	$('#menu_films_debutantes').css({"display":"none"});
	$('#link_mostra_debutantes').removeClass("sel");
}
function mostradebutantes(){
	$('#menu_films_casamento').css({"display":"none"});
	$('#link_mostra_casamento').removeClass("sel");

	$('#menu_films_debutantes').css({"display":"block"});
	$('#link_mostra_debutantes').addClass("sel");
}
function exibefilme(video,id){
	id_atual = $('#id_video').val();
	// return console.log(video);

	insere_html = '<input type="hidden" value="'+id+'" id="id_video"/>';
	// insere_html += '<object width="640" height="390"><param name="movie" value="//www.youtube.com/v/'+video+'"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/'+video+'?hl=pt_BR&amp;version=3" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>';
	// insere_html += '<object width="640" height="390"><param name="movie" value="'+video+'"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="'+video+'" type="application/x-shockwave-flash" width="640" height="390" allowscriptaccess="always" allowfullscreen="true"></embed></object>';
	insere_html += '<div class="video"><iframe '+
				'width="640" '+
				'height="390" '+
				'src="'+video+'" '+
				'frameborder="0" '+
				'allowfullscreen></iframe></div>';

	$('#content_interna_films_left').html(insere_html);

	$('#menu_films_casamento_'+id_atual).removeClass("sel");
	$('#menu_films_casamento_'+id).addClass("sel");
}
function exibeimagem(imagem,id,is_galeria){
	id_atual = $('#id_video').val();
	is_galeria = is_galeria || false;

	insere_html = '<input type="hidden" value="'+id+'" id="id_video"/>';
	insere_html += (is_galeria) ?
					'<div class="div_albuns prewedding"><a href="javascript:;" onClick="abrirgaleria('+id+')"><div class="over"></div><img src="'+PUBLIC_PATH+'/upload/prewedding/'+imagem+'"></a></div>' :
					'<img src="'+PUBLIC_PATH+'/upload/prewedding/'+imagem+'" alt="'+imagem+'" />';

	$('#content_interna_films_left').html(insere_html);

	$('#menu_films_casamento_'+id_atual).removeClass("sel");
	$('#menu_films_casamento_'+id).addClass("sel");
}